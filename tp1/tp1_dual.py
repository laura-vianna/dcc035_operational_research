import numpy as np
import sympy as sp
import utils

""" (iii) Given a matix in tableau and the number of lines in A
(which is the same number of columns of the left matrix in the tableau)
Calculate a line and column among the pivot candidates, for primal or dual simplex"""
def choose_dual_pivot(tableau_matrix, A_lines):
    # The limit of the A matrix columns is [1st element after the left matrix, one element before B]
    A_first_column = A_lines
    A_last_column = utils.columns(tableau_matrix)-1
    pivot_candidates = []

    # For each element in B vector (from the second to the last line, of the last column)
    for curr_line in range(1, utils.lines(tableau_matrix)):
        # If the current B element is negative
        if tableau_matrix[curr_line,-1:] < 0:
            # Cycle through all elements in A matrix, in the same line
            for curr_column in range(A_first_column, A_last_column):
                C_item = tableau_matrix[0, curr_column]
                A_item = tableau_matrix[curr_line, curr_column]

                # If a negative A element is in the same column of a positive tableau C item
                # The ratio will be calculated and added to a list with tuples (ratio, B line, A and C column)
                if A_item < 0 and C_item >= 0:
                    curr_ratio = C_item/(-A_item)
                    pivot_candidates.append((curr_ratio, curr_line ,curr_column))

    # If no viable ratio is found for all elements of A and C, a stop condition is signalized
    if len(pivot_candidates) == 0:
        return(-1, -1)

    # Else, find the minimum ratio in the list and return it's line and column
    min_ratio = min(pivot_candidates, key = lambda t: t[0])
    return(min_ratio[1], min_ratio[2])



def solve_by_dual_simplex(matrix, A_lines, A_columns):
    stop_condition = False
    while not stop_condition:
        P_line, P_column = choose_dual_pivot(matrix, A_lines)

        if P_line == -1:
            stop_condition = True
            break

        matrix = utils.pivot_matrix(matrix, P_line, P_column)
        utils.print_simplex_iteration(utils.ITERATIONS_FILE, matrix)

    B_is_positive = np.all(utils.B_vector(matrix) >= 0)

    utils.debug('Final matrix')
    utils.debug_matrix(matrix)

    # If a stop condition is achieved but B has a negative element, the PL is utils.INFEASIBLE
    if stop_condition and not B_is_positive:
        utils.debug("DUAL simplex - infeasible")
        utils.print_infeasible(matrix, A_lines)
        return utils.INFEASIBLE

    utils.debug("DUAL simplex - limited, optimal")
    utils.print_optimal(matrix, A_lines, A_columns)
    return utils.OPTIMAL
