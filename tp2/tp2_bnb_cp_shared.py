import utils
import sympy as sp
import numpy as np

def add_new_integer_restriction(matrix, source_line):
    base = utils.find_basic_columns(matrix)[1]
    new_matrix = np.vstack([matrix, source_line])

    last_I_column = np.eye(utils.lines(new_matrix), dtype=sp.Rational)[:, -1:]
    A_matrix = new_matrix[:, :utils.columns(matrix)-1]
    B_vector = new_matrix[:,-1:]
    concat_matrix = np.concatenate((A_matrix, last_I_column, B_vector), axis=1)
    utils.debug("concat_restriction_matrix")
    utils.debug(concat_matrix)

    final_matrix = put_matrix_in_canonical_format(concat_matrix, base, utils.lines(concat_matrix)-1)
    utils.debug("canonical_matrix_with_restriction")
    utils.debug(final_matrix)

    return final_matrix

def put_matrix_in_canonical_format(matrix, base, new_line_index):
    for curr_column in base:
        factor = matrix[new_line_index, curr_column]
        if factor != 0:
            for curr_line in range(1, utils.lines(matrix)):
                if matrix[curr_line, curr_column] == 1:
                    matrix[new_line_index, :] -= factor*matrix[curr_line, :]

    return matrix

""" (iii) These methods were built treat and format data to be printed in the output files
-> print_simplex_iteration method opens the specified output file "simplex_iterations.txt"
And print pivot operation results with the specified precision and format
(The Rational numbers are transformed into float)

The other method writes the "conclusao.txt" file as required:
a) print_infeasible
0

b) print_unbounded
1
[PI_certificate]

c) print_optimal
2
[PI_solution]
PI_target_value
[PL_solution]
PL_target_value
[PL_certificate]
"""
def build_unbounded_certificate(final_matrix, negative_column_index, certificate_items):
    first_A_item = certificate_items
    last_A_item = utils.columns(final_matrix)-1
    certificate = np.array([], dtype=sp.Rational)

    # For each column in the A matrix
    for curr_column in range(first_A_item, last_A_item):
        # If the current column is the one who signaled the utils.UNBOUNDED condition (all elements <=0)
        if curr_column is negative_column_index:
            # The value one will be in the certificate
            certificate = np.append(certificate, 1)
        # Else, verifies if the current column is a basic one
        else:
            is_basic, one_value_line_index = utils.check_if_column_basic(final_matrix, curr_column)
            # If the column is basic
            if is_basic:
                # The complement of the element in the problem column is added to the certificate_items
                # this element is (-1)*matrix[corresponding one value in the basic column index, problem negative column index]
                certificate = np.append(certificate, -1*final_matrix[one_value_line_index, negative_column_index])
            else:
                certificate = np.append(certificate, 0)
    if utils.DEBUG:
        positive_d = np.all(certificate >= 0)
        utils.debug("d >= 0") if positive_d else utils.debug("d < 0")

        A_matrix =  final_matrix[1:, first_A_item:last_A_item]
        Ad = np.dot(A_matrix, certificate)
        Ad_is_zero = np.all(Ad == 0)
        utils.debug("A*d = 0") if Ad_is_zero else utils.debug("A*d != 0")

        minus_C_vector = utils.C_vector_from_tableau(final_matrix, certificate_items) * -1
        Ct_times_d = np.dot(minus_C_vector.T, certificate)
        positive_Ctd = np.all(Ct_times_d > 0)
        utils.debug("c^T*d > 0") if positive_Ctd else utils.debug("c^T*d <= 0")

        utils.debug("Has valid certificate") if (positive_d and Ad_is_zero and positive_Ctd) else utils.debug("Invalid certificate")

    return certificate

def print_infeasible():
    file = open(utils.CONCLUSION_FILE, "w")
    utils.debug("print_conclusion_file - utils.INFEASIBLE "+ str(utils.INFEASIBLE))
    file.write(str(utils.INFEASIBLE)+'\n')
    file.close()

def print_unbounded(matrix, A_lines, negative_column_index):
    file = open(utils.CONCLUSION_FILE, "w")
    certificate = build_unbounded_certificate(matrix, negative_column_index, A_lines)
    certificate_as_str = utils.np_array_to_string(certificate)

    utils.debug("print_conclusion_file - utils.UNBOUNDED "+ str(utils.UNBOUNDED))
    utils.debug("[certificate] "+ certificate_as_str)
    file.write(str(utils.UNBOUNDED)+'\n')
    file.write(certificate_as_str)
    file.close()

def solution_and_certificate_to_str(PL_solution, PI_solution, certificate):
    certificate_as_str = utils.np_array_to_string(certificate)
    str_PL_solution = utils.np_array_to_string(PL_solution, utils.PRECISION_DECIMALS_SOLUTION, utils.PRINT_FORMAT_SOLUTION)
    str_PI_solution = utils.np_array_to_string(PI_solution, utils.PRECISION_DECIMALS_SOLUTION, utils.PRINT_FORMAT_SOLUTION)
    return str_PL_solution, str_PI_solution, certificate_as_str

def get_optimal_problem_information(matrix, A_lines, A_columns, PL_solution, PL_value, PI_equals_PL):
    if PI_equals_PL:
        return PL_solution, PL_value

    PI_solution = utils.has_integer_solution(matrix, A_columns)[1]
    PI_value = utils.get_target_value(matrix)
    return PI_solution, PI_value

def print_optimal(str_PL_solution, str_PI_solution, certificate_as_str, PL_value, PI_value):
    file = open(utils.CONCLUSION_FILE, "w")

    utils.debug("print_conclusion_file - utils.OPTIMAL "+ str(utils.OPTIMAL))
    utils.debug("[PI_solution]")
    utils.debug(str_PI_solution)
    utils.debug("PI_target_value")
    utils.debug(str(utils.PRINT_FORMAT_TARGET_VALUE % PI_value)+'\n')
    utils.debug("[PL_solution]")
    utils.debug(str_PL_solution)
    utils.debug("PL_target_value")
    utils.debug(str(utils.PRINT_FORMAT_TARGET_VALUE % PL_value)+'\n')
    utils.debug("[certificate] "+ certificate_as_str)

    file.write(str(utils.OPTIMAL)+'\n')
    file.write(str_PI_solution+'\n')
    file.write(str(utils.PRINT_FORMAT_TARGET_VALUE % PI_value)+'\n')
    file.write(str_PL_solution+'\n')
    file.write(str(utils.PRINT_FORMAT_TARGET_VALUE % PL_value)+'\n')
    file.write(certificate_as_str)
    file.close()
