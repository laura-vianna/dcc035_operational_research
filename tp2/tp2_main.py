"""
Aluna: LAURA VIANNA LUCCIOLA - 2013007544
Disciplina: DCC035 - Pesquisa Operacional
Trabalho Pratico 2
"""

""" Suggested Python library NumPy
Reference: http://cs231n.github.io/python-numpy-tutorial/#numpy
Instead of SciPy's fraction, this project will use the class
Rational() from SymPy library, due it's better compatibility with NumPy Arrays
"""
import numpy as np
import sympy as sp
# Necessary for I/O operatins and error handling
import sys
# This file contains all functions and variables that are common to all methods
import utils

import tp2_solve_linear_relaxation as PL_solver
import tp2_cutting_planes as cp
import tp2_branch_and_bound as bnb
import tp2_bnb_cp_shared as shared


""" (i) Given a text file name, through the command line, parse the read values into:
PI_mode - integer (0 or 1)\n A_lines - integer\n A_columns - integer\n
PL_matrix - NumPy Array where each element is a SymPy Rational"""
def read_input_file():
    # Verifies if the input file name was given when calling the program
    if len(sys.argv) < 2:
        print('[ERROR] Wrong number of arguments! Program must call "python3 tp2 <FILENAME>"')
        sys.exit(0)

    filename = sys.argv[1]

    # Treats case when file name is given with or without extension
    if not filename.endswith('.txt'):
        filename += '.txt'

    # Opens the input file and reads values according to the specification
    try:
        input_file = open(filename)
        PI_mode = int(input_file.readline())
        A_lines = int(input_file.readline())
        A_columns = int(input_file.readline())
        matrix = input_file.readline()
        input_file.close()
    # Treats case when file is not found
    except IOError:
        print('[ERROR] There was an error opening the file! Check if it exists and if the filename is written correctly.')
        sys.exit(0)

    try:
        PL_matrix = np.array(eval(matrix), dtype=sp.Rational)
        return PI_mode, A_lines, A_columns, PL_matrix
    # Treats case when file has a invalid character (that cannot be "eval")
    except SyntaxError:
        print('[ERROR] There was an error while reading the file! Please check if all charcters are valid')
        sys.exit(0)

def delete_left_matrix(matrix, A_lines):
    return matrix[:, A_lines:]

def get_certificate_value_from_PL(matrix, A_lines, A_columns):
    certificate = utils.get_certificate_from_tableau(matrix, A_lines)
    full_solution = utils.has_basic_solution(matrix, A_lines)[1]
    solution = full_solution[0:A_columns]
    utils.debug("PL - full_solution")
    utils.debug_matrix(full_solution)
    utils.debug("PL - solution")
    utils.debug_matrix(solution)
    target_value = utils.get_target_value(matrix)
    return certificate, solution, target_value

""" (ii) After reading the input file, the program will execute according to PI_mode:
0 - Cutting Planes method
1 - Branch and Bound Method"""
def solve_integer_problem_matrix(matrix, PI_mode, A_lines, A_columns):
    # (1) solve the linear relaxation (PL)
    PL_result, PL_matrix = PL_solver.solve_standart_format_matrix(matrix, A_lines, A_columns)

    # If the PL is INFEASIBLE or UNBOUNDED, the conclusion file can be printed already
    if PL_result is utils.INFEASIBLE:
        shared.print_infeasible()
        return utils.INFEASIBLE
    elif PL_result is utils.UNBOUNDED:
        # If the PL is UNBOUNDED, the certificate will be printed in "conclusao.txt" after the primal simplex
        return utils.UNBOUNDED

    PL_certificate, PL_solution, PL_value = get_certificate_value_from_PL(PL_matrix, A_lines, A_columns)
    # (2) Verifying if the PL solution is alredy integer
    int_solution, _ = utils.has_integer_solution(PL_matrix, A_columns, A_lines)

    if int_solution:
        utils.debug("The PL solution is integer, therefore PI is solved")
        PI_solution, PI_value = shared.get_optimal_problem_information(
            matrix, A_lines, A_columns, PL_solution, PL_value, True
        )
        str_PL_solution, str_PI_solution, certificate_as_str = shared.solution_and_certificate_to_str(PL_solution, PI_solution, PL_certificate)
        shared.print_optimal(str_PL_solution, str_PI_solution, certificate_as_str, PL_value, PI_value)
        return utils.OPTIMAL
    else:
        utils.debug("The PL solution is not integer")
        PI_matrix = delete_left_matrix(PL_matrix, A_lines)

        if PI_mode == 0:
            utils.debug("Solve by CUTTING PLANES")
            CP_result, PI_solution, PI_value = cp.cutting_planes_solver(PI_matrix, A_columns, A_lines)
            if CP_result is utils.INFEASIBLE:
                shared.print_infeasible()
                return utils.INFEASIBLE
            else:
                str_PL_solution, str_PI_solution, certificate_as_str = shared.solution_and_certificate_to_str(PL_solution, PI_solution, PL_certificate)
                shared.print_optimal(str_PL_solution, str_PI_solution, certificate_as_str, PL_value, PI_value)
                return utils.OPTIMAL
        elif PI_mode == 1:
            utils.debug("Solve by BRANCH AND BOUND")
            BNB_result, PI_solution, PI_value = bnb.branch_and_bound_solver(PI_matrix, A_columns, PL_solution)
            if BNB_result is False:
                shared.print_infeasible()
                return utils.INFEASIBLE
            else:
                str_PL_solution, str_PI_solution, certificate_as_str = shared.solution_and_certificate_to_str(PL_solution, PI_solution, PL_certificate)
                shared.print_optimal(str_PL_solution, str_PI_solution, certificate_as_str, PL_value, PI_value)
                return utils.OPTIMAL
        else:
            print('[ERROR] Unknown Integer Programming mode! Must be 0 or 1')


""" Main function """
PI_mode, A_lines, A_columns, PL_matrix = read_input_file()
utils.debug('PL_matrix')
utils.debug_matrix(PL_matrix)

SF_matrix = PL_solver.matrix_to_standart_format(PL_matrix, A_lines, A_columns)
solve_integer_problem_matrix(SF_matrix, PI_mode, A_lines, A_columns)
