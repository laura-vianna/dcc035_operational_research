import utils
import sympy as sp
import numpy as np
import tp2_bnb_cp_shared as shared
import tp2_dual


""" Integer programming solver - Branch And Bound """
def create_restrictions_from_solution(matrix, solution):
    floor_restriction = np.zeros((utils.columns(matrix)), dtype=sp.Rational)
    ceiling_restriction = np.zeros((utils.columns(matrix)), dtype=sp.Rational)
    has_fractional_x = False
    for index, value in enumerate(solution):
        # Get a Xi variable which the value is fractional
        if not utils.rational_is_int(value):
            # Create a restriction with Xi <= floor(Xi)
            floor_restriction[index] = sp.Rational(1,1)
            floor_num = sp.nsimplify(sp.floor(value), rational=True, tolerance=utils.DEBUG_TOLERANCE)
            floor_restriction[-1] = floor_num
            # Create a restriction with Xi >= ceiling(Xi) --> -Xi <= -ceiling(Xi)
            ceiling_restriction[index] = sp.Rational(-1,1)
            ceiling_num = sp.nsimplify(-sp.ceiling(value), rational=True, tolerance=utils.DEBUG_TOLERANCE)
            ceiling_restriction[-1] = ceiling_num
            has_fractional_x = True
            break

    return has_fractional_x, floor_restriction, ceiling_restriction

def dual_simplex_for_branch_and_bound(matrix, A_columns):
    simplex_result, final_matrix = tp2_dual.solve_by_dual_simplex(matrix)
    if simplex_result is utils.INFEASIBLE:
        return simplex_result, [], 0
    target_value = utils.get_target_value(final_matrix)
    simplex_result, solution = utils.has_integer_solution(final_matrix, A_columns)
    return simplex_result, solution, target_value

def branch_problems(matrix, A_columns, restriction):
    # PI_1 + restriction 1.1 + gap variable; putting in canonical format
    new_matrix = shared.add_new_integer_restriction(matrix, restriction)

    # Solve with dual simplex
    result, solution, target_value = dual_simplex_for_branch_and_bound(new_matrix, A_columns)
    utils.debug("Dual simplex results - target value "+ str(target_value))
    utils.debug_matrix(solution)
    return {'type':result,'solution': solution, 'target_value': target_value, 'matrix': new_matrix }

def add_problem_to_stack(stack, data, max_target_value):
    # If the problem simplex result was INFEASIBLE, no need to continue branching this node - PRUNE
    if data['type'] is not utils.INFEASIBLE:
        if data['target_value'] >= max_target_value:
            stack.append(data)

def branch_and_add_to_stack(matrix, A_columns, solution, stack, max_target_value):
    has_fractional_x, floor_restriction, ceiling_restriction = create_restrictions_from_solution(matrix, solution)
    utils.debug("New restrictions - floor and ceiling")
    utils.debug_matrix(floor_restriction)
    utils.debug_matrix(ceiling_restriction)

    if has_fractional_x:
        utils.debug("Branched problems - floor and ceiling")
        f_data = branch_problems(matrix, A_columns, floor_restriction)
        utils.debug_dict(f_data)
        c_data = branch_problems(matrix, A_columns, ceiling_restriction)
        utils.debug_dict(c_data)
    else:
        utils.debug("Branched problems - No fractional solution")

    add_problem_to_stack(stack, f_data, max_target_value)
    add_problem_to_stack(stack, c_data, max_target_value)

    return stack

def branch_and_bound_solver(matrix, A_columns, PL_solution):
    max_target_value = 0
    max_solution = []
    integer_solution = False
    stop_condition = False

    # To avoid a recursive implementation, a Stack ("last-in, first-out") data structure will be used
    # To add an item to the top of the stack, use append().
    # To retrieve an item from the top of the stack, use pop() without an explicit index.
    subproblem_stack = []
    branch_and_add_to_stack(matrix, A_columns, PL_solution, subproblem_stack, max_target_value)

    while len(subproblem_stack) > 0:
        curr_problem = subproblem_stack.pop()

        # If solution is integer
        if curr_problem['type'] is True:
            # and target value > max_target_value, it becomes the maximum target value
            if curr_problem['target_value'] > max_target_value:
                # And there is no need to continue branching this node - PRUNE
                max_target_value = curr_problem['target_value']
                max_solution = curr_problem['solution']
                integer_solution = True
            else:
                branch_and_add_to_stack(
                    curr_problem['matrix'], A_columns, curr_problem['solution'], subproblem_stack, max_target_value
                )
        # If solution is fractional
        if curr_problem['type'] is False:
            if curr_problem['target_value'] > max_target_value:
                branch_and_add_to_stack(
                    curr_problem['matrix'], A_columns, curr_problem['solution'], subproblem_stack, max_target_value
                )

    return integer_solution, max_solution, max_target_value
