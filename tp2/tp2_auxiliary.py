import numpy as np
import sympy as sp
import sys
import utils
import tp2_primal

# Create the extended "obvious basic solution" matrix, which is formed by:
# 0 ... 0 | 0 ... 0 -1 ... -1| 0
# I matrix|     A I matrix   | B
def build_auxiliary_matrix(matrix, A_lines):
    # Creates a "obvious basic solution" matrix, with a all -1 header above a Identity matrix
    I_matrix_header = np.full((1, A_lines), 1, dtype=sp.Rational)
    I_matrix = np.eye(A_lines, dtype=sp.Rational)
    extended_I_matrix = np.concatenate((I_matrix_header, I_matrix), axis=0)

    # Transform the given C vector in a vector of 0s
    num = len(utils.C_vector_from_tableau(matrix, A_lines))
    new_C_vector = np.zeros((1, num), dtype=sp.Rational)
    matrix[0,:-A_lines-1] = new_C_vector

    # Inserts the "obvious basic solution" matrix between A matrix and B vector
    extended_A_matrix = matrix[:, :utils.columns(matrix)-1]
    extended_B_vector = matrix[:,-1:]
    auxiliary_matrix = np.concatenate((extended_A_matrix, extended_I_matrix, extended_B_vector), axis=1)

    return auxiliary_matrix

def auxiliary_to_canonical_format(matrix, A_lines):
    # Adds each line to the C vector, to make all elements in auxiliary Identity matrix zero
    for curr_line in range(1, utils.lines(matrix)):
        matrix[0, :] -= matrix[curr_line, :]
    return matrix

# Given a matrix and a known basic solution base, puts the matrix in canonical form
def solved_aux_matrix_to_canonical_format(matrix, A_lines, base):
    for curr_column in base:
        constant = -matrix[0, curr_column]
        for curr_line in range(1, utils.lines(matrix)):
            if matrix[curr_line, curr_column] == 1:
                matrix[0, :] += (matrix[curr_line, :]*constant)
    return matrix

def solve_auxiliary_matrix(matrix, A_lines):
    stop_condition = False

    while not stop_condition:
        positive_C = np.all(utils.C_vector_from_tableau(matrix, A_lines) >= 0)

        # Verifies if the matrix is already in stop condition for auxiliary simplex
        if positive_C:
            stop_condition = True
            break

        P_line, P_column, result = tp2_primal.choose_primal_pivot(matrix, A_lines)

        # Verifies if the matrix is in stop condition
        if P_line is -1 :
            if result is utils.UNBOUNDED:
                continue
            else:
                stop_condition = True
                break

        matrix = utils.pivot_matrix(matrix, P_line, P_column)
        utils.print_simplex_iteration(utils.ITERATIONS_FILE, matrix)

    utils.debug('Final aux matrix')
    utils.debug_matrix(matrix)
    target_value = utils.get_target_value(matrix)

    return target_value, matrix

def auxiliary_simplex_method(matrix, A_lines):
    aux_matrix = build_auxiliary_matrix(matrix, A_lines)

    canonical_matrix = auxiliary_to_canonical_format(aux_matrix, A_lines)
    utils.debug('canonical_matrix')
    utils.debug_matrix(canonical_matrix)

    primal_target_value, final_matrix = solve_auxiliary_matrix(canonical_matrix, A_lines)

    if primal_target_value >= 0:
        utils.debug("AUXILIARY simplex - limited, optimal")
        return utils.OPTIMAL, final_matrix

    utils.debug("AUXILIARY simplex - infeasible")
    return utils.INFEASIBLE, final_matrix


def get_basic_solution_from_auxiliary(final_aux_matrix, initial_C_line, A_lines):
    # Deletes columns from extended Identity matrix in auxiliary
    extra_columns = list(range((utils.columns(final_aux_matrix)-A_lines-1),(utils.columns(final_aux_matrix)-1)))
    tableau_matrix = np.delete(final_aux_matrix, extra_columns, 1)

    # Substitutes the C vector by the initial one
    tableau_matrix[0, :] = initial_C_line
    return tableau_matrix
